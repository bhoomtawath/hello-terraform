resource "aws_eks_cluster" "hello" {
  name = "${var.cluster-name}"
  role_arn = "${aws_iam_role.hello-cluster.arn}"

  vpc_config {
    security_group_ids = ["${aws_security_group.hello-cluster.id}"]
    subnet_ids = ["${aws_subnet.hello.*.id}"]
  }

  depends_on = [
    "aws_iam_role_policy_attachment.hello-cluster-AmazonEKSClusterPolicy",
    "aws_iam_role_policy_attachment.hello-cluster-AmazonEKSServicePolicy"
  ]
}
