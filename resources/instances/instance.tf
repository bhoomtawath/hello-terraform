resource "aws_instance" "hello" {
  ami = "ami-07a76b70f6e97be33"
  instance_type = "t2.micro"

  tags = {
    Name = "hello-terraform"
  }
}
